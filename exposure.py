"""
    klayout-raith
    Copyright (C) 2016  Jakub Sadílek, Jiří Babocký

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import pya
import math

class Exposure(pya.PCellDeclarationHelper):
  """
  The PCell declaration for the exposure
  """

  def __init__(self):

    # Important: initialize the super class
    super(Exposure, self).__init__()

    # declare the parameters
    #basic
    self.param("l", self.TypeLayer, "Layer", default = pya.LayerInfo(1, 0))
    self.param("s", self.TypeShape, "", default = pya.DPoint(0, 0))
    #geometry
    self.param("w", self.TypeDouble, "Width", default=10.0)
    self.param("h", self.TypeDouble, "Height", default=10.0)
    #previous
    self.param("wu", self.TypeDouble, "Width", default = 0.0, hidden = True)
    self.param("hu", self.TypeDouble, "Width", default = 0.0, hidden = True)
    #Exposure params
    self.param("layers", self.TypeString, "Exposed layers", default="")
    self.param("adefault", self.TypeBoolean, "Use global settings", default=True)
    self.param("adwell", self.TypeDouble, "Area dwell time", default=10.0)
    self.param("astepu", self.TypeDouble, "Area step size U", default=10.0)
    self.param("astepv", self.TypeDouble, "Area step size V", default=10.0)
    self.param("cpattern", self.TypeBoolean, "Pattern curved elements", default=True)
    self.param("cdefault", self.TypeBoolean, "Use global settings", default=True)
    self.param("cdwell", self.TypeDouble, "Curved element dwell time", default=10.0)
    self.param("cstepu", self.TypeDouble, "Curved element step size", default=10.0)
    self.param("cstepv", self.TypeDouble, "Curved element line step size", default=10.0)
    self.param("spattern", self.TypeBoolean, "Pattern SPL", default=True)
    self.param("sdefault", self.TypeBoolean, "Use global settings", default=True)
    self.param("sdwell", self.TypeDouble, "SPL dwell time", default=10.0)
    self.param("sstep", self.TypeDouble, "SPL step size", default=10.0)
    self.param("ppattern", self.TypeBoolean, "Pattern points", default=True)
    self.param("pdefault", self.TypeBoolean, "Use global settings", default=True)
    self.param("pdwell", self.TypeDouble, "Point dwell time", default=10.0)

  def display_text_impl(self):
    # Provide a descriptive text for the cell
    return "Exposure"
  
  def coerce_parameters_impl(self):
  
    # We employ coerce_parameters_impl to decide whether the handle or the 
    # numeric parameter has changed (by comparing against the effective 
    # radius ru) and set ru to the effective radius. We also update the 
    # numerical value or the shape, depending on which on has not changed.
    ws = None
    hs = None
    if isinstance(self.s, pya.DPoint): 
      ws = self.s.x
      hs = self.s.y
    if (ws != None and abs(self.w-self.wu) < 1e-6) and (hs != None and abs(self.h-self.hu) < 1e-6):
      self.wu = ws
      self.w = ws 
      self.hu = hs
      self.h = hs
    else:
      self.wu = self.w
      self.hu = self.h
      self.s = pya.DPoint(self.wu, self.hu)
  
  def can_create_from_shape_impl(self):
    # Implement the "Create PCell from shape" protocol: we can use any shape which 
    # has a finite bounding box
    return self.shape.is_box() or self.shape.is_polygon() or self.shape.is_path()
  
  def parameters_from_shape_impl(self):
    # Implement the "Create PCell from shape" protocol: we set r and l from the shape's 
    # bounding box width and layer
    self.w = self.shape.bbox().width() * self.layout.dbu
    self.h = self.shape.bbox().height() * self.layout.dbu
    self.l = self.layout.get_info(self.layer)
  
  def transformation_from_shape_impl(self):
    # Implement the "Create PCell from shape" protocol: we use the center of the shape's
    # bounding box to determine the transformation
    return pya.Trans(self.shape.bbox().left, self.shape.bbox().bottom)
  
  def produce_impl(self):
  
    # This is the main part of the implementation: create the layout

    # fetch the parameters
    w_dbu = self.wu / self.layout.dbu
    h_dbu = self.hu / self.layout.dbu
    
    # create the shape
    points = []
    points.append(pya.Point(0,0))
    points.append(pya.Point(w_dbu,0))
    points.append(pya.Point(w_dbu,h_dbu))
    points.append(pya.Point(0,h_dbu))
    points.append(pya.Point(0,0))
    self.cell.shapes(self.l_layer).insert(pya.Path(points,0))
    area = ""
    if self.adefault:
      area = "default"
    else:
      area = "d"+str(self.adwell)+" u"+str(self.astepu)+" v"+str(self.astepv)
    curve = ""
    if self.cdefault:
      curve = "default"
    else:
      curve = "d"+str(self.cdwell)+" u"+str(self.cstepu)+" v"+str(self.cstepv)
    spl = ""
    if self.sdefault:
      spl = "default"
    else:
      spl = "d"+str(self.sdwell)+" u"+str(self.sstep)
    point = ""
    if self.pdefault:
      point = "default"
    else:
      point = "d"+str(self.pdwell)
    text = "Layers " + self.layers
    text += "\nAreas "+area
    if self.cpattern:
      text += "\nCurved el. "+curve
    if self.spattern:
      text += "\nSPL "+spl
    if self.ppattern:
      text += "\nPoints "+point

    self.cell.shapes(self.l_layer).insert(pya.Text(text, 0, 0))