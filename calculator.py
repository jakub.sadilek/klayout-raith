"""
    klayout-raith
    Copyright (C) 2016  Jakub Sadílek, Jiří Babocký

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import pya
from pya import *
import types
import os
import json
import math

#togglers
def areaDefault_toggled(self, value):
  self.areaL1.setDisabled(value)
  self.areaL3.setDisabled(value)
  self.areaL4.setDisabled(value)
  self.areaL5.setDisabled(value)
  self.areaStep.setDisabled(value)
  self.areaDwell.setDisabled(value)
  self.areaDose.setDisabled(value)
  self.calcAreaStep.setDisabled(value)
  self.calcAreaDwell.setDisabled(value)
  self.calcAreaDose.setDisabled(value)
  self.areaEqual.setDisabled(value)
  if value or self.areaEqual():
    self.areaL2.setDisabled(True)
    self.areaSpacing.setDisabled(True)
    self.calcAreaSpacing.setDisabled(True)
  else:
    self.areaL2.setDisabled(False)
    self.areaSpacing.setDisabled(False)
    self.calcAreaSpacing.setDisabled(False)

def curveEnabled_toggled(self, value):
  self.curDefault.setEnabled(self.curEnabled.isChecked())
  result = self.curEnabled.isChecked() and not self.curDefault.isChecked()
  self.curL1.setEnabled(result)
  self.curL3.setEnabled(result)
  self.curL4.setEnabled(result)
  self.curL5.setEnabled(result)
  self.curStep.setEnabled(result)
  self.curDwell.setEnabled(result)
  self.curDose.setEnabled(result)
  self.calcCurStep.setEnabled(result)
  self.calcCurDwell.setEnabled(result)
  self.calcCurDose.setEnabled(result)
  self.curEqual.setEnabled(result)
  result = result and not self.curEqual.isChecked()
  self.curL2.setEnabled(result)
  self.curSpacing.setEnabled(result)
  self.calcCurSpacing.setEnabled(result)

def splEnabled_toggled(self, value):
  self.splDefault.setEnabled(self.splEnabled.isChecked())
  result = self.splEnabled.isChecked() and not self.splDefault.isChecked()
  self.splL1.setEnabled(result)
  self.splL2.setEnabled(result)
  self.splL3.setEnabled(result)
  self.splL4.setEnabled(result)
  self.splStep.setEnabled(result)
  self.splDwell.setEnabled(result)
  self.splDose.setEnabled(result)
  self.calcSplStep.setEnabled(result)
  self.calcSplDwell.setEnabled(result)
  self.calcSplDose.setEnabled(result)
  self.curEqual.setEnabled(result)

def dotEnabled_toggled(self, value):
  self.dotDefault.setEnabled(self.dotEnabled.isChecked())
  result = self.dotEnabled.isChecked() and not self.dotDefault.isChecked()
  self.dotL1.setEnabled(result)
  self.dotL2.setEnabled(result)
  self.dotDwell.setEnabled(result)
  self.dotDose.setEnabled(result)
  self.calcDotDwell.setEnabled(result)
  self.calcDotDose.setEnabled(result)

def adjustAreaSpacing(self, val):
  if self.areaEqual.isChecked():
    self.areaSpacing.setValue(self.areaStep.value)

def adjustCurSpacing(self, val):
  if self.curEqual.isChecked():
    self.curSpacing.setValue(self.curStep.value)
  
#getters/setters
def areaParams(self):
  step = self.areaStep.value*1e-6
  spacing = self.areaSpacing.value*1e-6
  dwell = self.areaDwell.value*1e-3
  dose  = self.areaDose.value*(1e-6/1e-4)
  return step, spacing, dwell, dose
  
def setAreaParams(self, step, spacing, dwell, dose):
  self.areaStep.setValue(step/1e-6)
  self.areaSpacing.setValue(spacing/1e-6)
  self.areaDwell.setValue(dwell/1e-3)
  self.areaDose.setValue(dose/(1e-6/1e-4))
  
def curParams(self):
  step = self.curStep.value*1e-6
  spacing = self.curSpacing.value*1e-6
  dwell = self.curDwell.value*1e-3
  dose  = self.curDose.value*(1e-6/1e-4)
  return step, spacing, dwell, dose

def setCurParams(self, step, spacing, dwell, dose):
  self.curStep.setValue(step/1e-6)
  self.curSpacing.setValue(spacing/1e-6)
  self.curDwell.setValue(dwell/1e-3)
  self.curDose.setValue(dose/(1e-6/1e-4))

def splParams(self):
  step = self.splStep.value*1e-6
  dwell = self.splDwell.value*1e-3
  dose  = self.splDose.value*1e-12/0.01
  return step, dwell, dose

def setSplParams(self, step, dwell, dose):
  self.splStep.setValue(step/1e-6)
  self.splDwell.setValue(dwell/1e-3)
  self.splDose.setValue(dose/1e-12*0.01)
  
def dotParams(self):
  dwell = self.dotDwell.value*1e-3
  dose  = self.dotDose.value*1e-12
  return dwell, dose

def setDotParams(self, dwell, dose):
  self.dotDwell.setValue(dwell/1e-3)
  self.dotDose.setValue(dose/1e-12)

def exposureParams(self):
  current = self.current.value*1e-9
  minStep = self.minStep.value*1e-6
  return current,minStep
  
def setCurrent(self, current):
  self.current.setValue(current/1e-9)

def setExposureParams(self, current, minStep):
  self.current.setValue(current/1e-9)
  self.minStep.setValue(minStep/1e-6)

#correcctors  
def correctDwell(self, dwell, index):
  selection = self.instrument.currentText
  if selection is not None:
    macroDir = QFileInfo(os.path.realpath(__file__)).dir().path
    instrDir = macroDir + "/../instruments/"
    file = QFile(instrDir + selection + ".inst")
    if file.open(QIODevice.ReadOnly):
      stream = QTextStream(file)
      data = json.loads(stream.readAll())
      index = index+"DwellLimit"
      if dwell < data[index]:
        dwell = data[index]
      file.close()
  return dwell

def correctStep(self, step):
  minStep = self.minStep.value*1e-6
  if minStep !=0:
    step = step+minStep-step%minStep;
    if step == 0:
      step = minStep
  return step

#calculators
def calcAreaDose_clicked(self, tmp):
  calcAreaDose(self, True)

def calcCurDose_clicked(self, tmp):
  calcAreaDose(self, False)

def calcAreaDose(self, area):
  current,minStep = exposureParams(self)
  step = spacing = dwell = dose = 0.0
  if area:
    step, spacing, dwell, dose = areaParams(self)
  else:
    step, spacing, dwell, dose = curParams(self)
  if step*spacing != 0:
    dose = (current*dwell)/(step*spacing)
    if area:
      setAreaParams(self, step, spacing, dwell, dose)
    else:
      setCurParams(self, step, spacing, dwell, dose)

def calcAreaDwell_clicked(self, tmp):
  calcAreaDwell(self, True)

def calcCurDwell_clicked(self, tmp):
  calcAreaDwell(self, False)
  
def calcAreaDwell(self, area):
  current,minStep = exposureParams(self)
  step = spacing = dwell = dose = 0.0
  if area:
    step, spacing, dwell, dose = areaParams(self)
  else:
    step, spacing, dwell, dose = curParams(self)
  step = correctStep(self, step)
  spacing = correctStep(self, spacing)
  if current != 0:
    dwell = dose*step*spacing/current
    oldDwell = dwell
    struct = ""
    if area:
      struct = "area"
    else:
      struct = "cur"
    dwell = correctDwell(self, dwell, struct)
    if area:
      setAreaParams(self, step, spacing, dwell, dose)
    else:
      setCurParams(self, step, spacing, dwell, dose)
    if oldDwell != dwell:
      msg = QMessageBox()
      msg.setText("Dwell time limit")
      msg.setInformativeText("Minimal dwell time limit reached, what to do?")
      msg.setIcon(QMessageBox.Warning)
      doseBtn = msg.addButton("Adjust dose", QMessageBox.YesRole)
      stepBtn = msg.addButton("Adjust step size", QMessageBox.NoRole)
      msg.exec_()
      if msg.clickedButton is doseBtn:
        calcAreaDose(self, area)
      else:
        calcAreaStep(self, area, True)
  calcAreaSpeed(self, area)

def calcAreaStep_clicked(self, tmp):
  calcAreaStep(self, True, True)

def calcCurStep_clicked(self, tmp):
  calcAreaStep(self, False, True)

def calcAreaSpacing_clicked(self, tmp):
  calcAreaStep(self, True, False)

def calcCurSpacing_clicked(self, tmp):
  calcAreaStep(self, False, False)

def calcAreaStep(self, area, step):
  current,minStep = exposureParams(self)
  step = spacing = dwell = dose = 0.0
  equal = False
  if area:
    step, spacing, dwell, dose = areaParams(self)
    equal = self.areaEqual.isChecked()
  else:
    step, spacing, dwell, dose = curParams(self)
    equal = self.curEqual.isChecked()
  if dose != 0:
    stepspacing = current*dwell/dose
    if equal:
      step = spacing = math.sqrt(stepspacing)
    else:
      if step and spacing != 0:
        step = stepscacing/spacing
      elif step != 0:
        spacing = stepspacing/step
    oldStep = step
    step = correctStep(self, step)
    setSplParams(self, step, dwell, dose)
    oldSpacing = spacing
    spacing = correctStep(self, spacing)
    if area:
      setAreaParams(self, step, spacing, dwell, dose)
    else:
      setCurParams(self, step, spacing, dwell, dose)
    if oldStep != step or oldSpacing != spacing:
      calcAreaDwell(self, area)
  calcSplSpeed(self)

def calcAreaSpeed(self, area):
  step, spacing, dwell, dose = areaParams(self)
  if dwell != 0:
    speed = step/dwell
    if area:
      self.areaSpeed.setValue(speed/1e-3)
    else:
      self.CurSpeed.setValue(speed/1e-3)

def calcSplDose_clicked(self, tmp):
  current,minStep = exposureParams(self)
  step,dwell, dose = splParams(self)
  step = correctStep(self, step)
  dose = (current*dwell)/step
  setSplParams(self, step, dwell, dose)
  calcSplSpeed(self)

def calcSplDwell_clicked(self, tmp):
  current,minStep = exposureParams(self)
  step, dwell, dose = splParams(self)
  step = correctStep(self, step)
  if current != 0:
    dwell = dose*step/current
    oldDwell = dwell
    dwell = correctDwell(self, dwell, "spl")
    setSplParams(self, step, dwell, dose)
    if oldDwell != dwell:
      msg = QMessageBox()
      msg.setText("Dwell time limit")
      msg.setInformativeText("Minimal spl dwell time limit reached, what to do?")
      msg.setIcon(QMessageBox.Warning)
      doseBtn = msg.addButton("Adjust dose", QMessageBox.YesRole)
      stepBtn = msg.addButton("Adjust step size", QMessageBox.NoRole)
      msg.exec_()
      if msg.clickedButton is doseBtn:
        calcSplDose_clicked(self, 1)
      else:
        calcSplStep_clicked(self, 1)
  calcSplSpeed(self)
        
def calcSplStep_clicked(self, tmp):
  current,minStep = exposureParams(self)
  step, dwell, dose = splParams(self)
  if dose != 0:
    step = current*dwell/dose
    oldStep = step
    step = correctStep(self, step)
    setSplParams(self, step, dwell, dose)
    if oldStep != step:
      calcSplDwell_clicked(self, 1)
  calcSplSpeed(self)

def calcSplSpeed(self):
  step, dwell, dose = splParams(self)
  if dwell != 0:
    speed = step/dwell
    self.splSpeed.setValue(speed/1e-3)

def calcDotDose_clicked(self, tmp):
  current,minStep = exposureParams(self)
  dwell, dose = dotParams(self)
  dose = current*dwell
  setDotParams(self, dwell, dose)

def calcDotDwell_clicked(self, tmp):
  current,minStep = exposureParams(self)
  dwell, dose = dotParams(self)
  if current != 0:
    dwell = dose/current
    oldDwell = dwell
    dwell = correctDwell(self, dwell, "dot")
    setDotParams(self, dwell, dose)
    if oldDwell != dwell:
      QMessageBox.warning(self, "Dwell time limit", "Minimal dot dwell time limit reached, adjusting dose...")
      calcDotDose_clicked(self, 1)  

def calcCurrent_clicked(self, tmp):
  if self.tabs.currentIndex == 0:
    current,minStep = exposureParams(self)
    step, spacing, dwell, dose = areaParams(self)
    if dwell != 0:
      current = dose*step*spacing/dwell
      setCurrent(self, current)
  elif self.tabs.currentIndex == 1:
    current,minStep = exposureParams(self)
    step, spacing, dwell, dose = curParams(self)
    if dwell != 0:
      current = dose*step*spacing/dwell
      setCurrent(self, current)
  elif self.tabs.currentIndex == 2:
    current,minStep = exposureParams(self)
    step, dwell, dose = splParams(self)
    if dwell != 0:
      current = dose*step/dwell
      setCurrent(self, current)
  elif self.tabs.currentIndex == 3:
    current,minStep = exposureParams(self)
    dwell, dose = dotParams(self)
    if dwell != 0:
      current = dose/dwell
      setCurrent(self, current)      
  
def loadData(self, tmp):
  filename = QFileDialog.getOpenFileName(self, "Load exposure preset", "", "Exposure settings (*.exp)")
  if filename != "":
    file = QFile(filename)
    file.open(QIODevice.ReadOnly)
    data = json.loads(file.readAll())
    file.close()
    setExposureParams(self, data["current"], data["minStep"])
    setAreaParams(self, data["areaStep"], data["areaSpacing"], data["areaDwell"], data["areaDose"])
    setCurParams(self, data["curStep"], data["curSpacing"], data["curDwell"], data["curDose"])
    setSplParams(self, data["splStep"], data["splDwell"], data["splDose"])
    setDotParams(self, data["dotDwell"], data["dotDose"])


def saveData(self, tmp):
  data = dict()
  current, minStep = exposureParams(self)
  data["current"] = current
  data["minStep"] = minStep
  astep, aspacing, adwell, adose = areaParams(self)
  data["areaStep"] = astep
  data["areaSpacing"] = aspacing
  data["areaDwell"] = adwell
  data["areaDose"] = adose
  cstep, cspacing, cdwell, cdose = curParams(self)
  data["curStep"] = cstep
  data["curSpacing"] = cspacing
  data["curDwell"] = cdwell
  data["curDose"] = cdose
  sstep, sdwell, sdose = splParams(self)
  data["splStep"] = sstep
  data["splDwell"] = sdwell
  data["splDose"] = sdose
  ddwell, ddose = dotParams(self)
  data["dotDwell"] = ddwell
  data["dotDose"] = ddose
  filename = QFileDialog.getSaveFileName(self, "Save exposure preset", "", "Exposure settings (*.exp)")
  if filename != "":
    file = QFile(filename)
    file.open(QIODevice.WriteOnly)
    file.write(json.dumps(data))
    file.close()

def show():
  thisScript = os.path.realpath(__file__)
  ui_file = QFile(QFileInfo(thisScript).dir().filePath("Calculator.ui"))
  ui_file.open(QIODevice.ReadOnly)
  form = QFormBuilder().load(ui_file, Application.instance().main_window())
  ui_file.close()
  
  macroDir = QFileInfo(os.path.realpath(__file__)).dir().path
  instrDir = macroDir + "/../instruments"
  if not QFile.exists(instrDir):
    klayoutDir = QDir(macroDir + "/..")
    klayoutDir.mkdir("instruments")
  dirObj = QDir(instrDir)
  dirObj.setNameFilters(["*.inst"])
  dirObj.setFilter(QDir.Files | QDir.NoDotAndDotDot | QDir.NoSymLinks)
  files = dirObj.entryList()
  for file in files:
    form.instrument.addItem(file[:-5])

  form.areaDefault.toggled(types.MethodType(areaDefault_toggled, form))
  form.curDefault.toggled(types.MethodType(curveEnabled_toggled, form))
  form.curEnabled.toggled(types.MethodType(curveEnabled_toggled, form))
  form.splDefault.toggled(types.MethodType(splEnabled_toggled, form))
  form.splEnabled.toggled(types.MethodType(splEnabled_toggled, form))
  form.dotDefault.toggled(types.MethodType(dotEnabled_toggled, form))
  form.dotEnabled.toggled(types.MethodType(dotEnabled_toggled, form))
  form.calcAreaDose.clicked(types.MethodType(calcAreaDose_clicked, form))
  form.calcCurDose.clicked(types.MethodType(calcCurDose_clicked, form))
  form.calcAreaStep.clicked(types.MethodType(calcAreaStep_clicked, form))
  form.calcCurStep.clicked(types.MethodType(calcCurStep_clicked, form))
  form.calcAreaDwell.clicked(types.MethodType(calcAreaDwell_clicked, form))
  form.calcCurDwell.clicked(types.MethodType(calcCurDwell_clicked, form))
  form.calcDotDose.clicked(types.MethodType(calcDotDose_clicked, form))
  form.calcDotDwell.clicked(types.MethodType(calcDotDwell_clicked, form))
  form.calcCurrent.clicked(types.MethodType(calcCurrent_clicked, form))
  form.calcSplDose.clicked(types.MethodType(calcSplDose_clicked, form))
  form.calcSplDwell.clicked(types.MethodType(calcSplDwell_clicked, form))
  form.calcSplStep.clicked(types.MethodType(calcSplStep_clicked, form))
  form.areaEqual.toggled(types.MethodType(adjustAreaSpacing, form))
  form.areaStep.valueChanged(types.MethodType(adjustAreaSpacing, form))
  form.curEqual.toggled(types.MethodType(adjustCurSpacing, form))
  form.curStep.valueChanged(types.MethodType(adjustCurSpacing, form))
  form.load.clicked(types.MethodType(loadData, form))
  form.save.clicked(types.MethodType(saveData, form))


  retval = form.exec_()
  areaDefault = form.areaDefault.isChecked()
  areaStep = form.areaStep.value
  areaSpacing = form.areaSpacing.value
  areaDwell = form.areaDwell.value
  curEnabled = form.curEnabled.isChecked()
  curDefault = form.curDefault.isChecked()
  curStep = form.curStep.value
  curSpacing = form.curSpacing.value
  curDwell = form.curDwell.value
  splEnabled = form.splEnabled.isChecked()
  splDefault = form.splDefault.isChecked()
  splStep = form.splStep.value
  splDwell = form.splDwell.value
  dotEnabled = form.dotEnabled.isChecked()
  dotDefault = form.dotDefault.isChecked()
  dotDwell = form.dotDwell.value
  
  if retval:
    return  areaDefault, areaStep, areaSpacing, areaDwell, curEnabled, curDefault, curStep, curSpacing, curDwell, splEnabled, splDefault, splStep, splDwell, dotEnabled, dotDefault, dotDwell
  else:
    return None