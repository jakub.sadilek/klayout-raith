Klayout-raith
=============

Raith toolbox for KLayout GDSII editor

Current Feautures
-----------------
* Design working areas and set-up exposure visually inside GDSII
* Automatically generate PLS files

Instalation
-----------
Copy all files to KLayout scripts folder

* Linux: ~/.klayout/pymacros
* Windows: C:\Users\user\KLayout\pymacros

Configuration
-------------
It is necessary to set limits of your pattern generator inside KLayout to let
the exposure calculator to calculate the parameters correctly. You can set these
limits using Tools -> Edit instruments.

To obtain these limits
* open the Raith software
* create some test structure with areas, curved elements, lines and dots
* put it in PLS
* start calculator, enter insanely small dose, small spacing and high beam current
* click on calculate, Raith SW will change your dose and set dwell time to its minimum limit